- 👋 Hi, I’m Jackson Montenegro
- 👀 I am interested in continuing to learn a lot about programming, especially mobile development with Java, as well as web developer with different frameworks.
- 🌱 I am currently reading about neural networks in Python as part of artificial intelligence.
- 💞️ I’m looking to collaborate on Hackathon Nicaragua in 2020 and 2021 with different projects de innovation
- 📫 How to reach me ...

<!---
MJackson22-bit/MJackson22-bit is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
